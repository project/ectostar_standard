<?php
/**
 * @file
 * Hooks provided by the Date module.
 *
 * Entityform and Entityform type are standard entities using the Entity API module.
 * The standard Entity hooks are availabe for them.
 * Including
 *  hook_conf_eform_load
 *  hook_conf_eform_insert
 *  hook_conf_eform_update
 *  hook_conf_eform_delete
 *  hook_conf_eform_presave
 *  hook_conf_eform_view
 *  hook_conf_eform_view_alter
 *  hook_conf_eform_type_load
 *  hook_conf_eform_type_insert
 *  hook_conf_eform_type_update
 *  hook_conf_eform_type_delete
 *  hook_conf_eform_type_presave
 *  hook_conf_eform_type_view
 *  hook_conf_eform_type_view_alter
 *
 *
 */
/**
 * Alter the render array that will make the confirm page all conf_eform types.
 *
 * This is the called the "Submission Page Settings" on the Entityform Type edit page.
 * @param $render_array
 * @param $conf_eform_type
 * @param $conf_eform_id
 */
function hook_conf_eform_confirm_page_alter(&$render_array, $conf_eform_type, $conf_eform_id) {
  $render_array['new_markup'] = array(
    '#markup' => t('Hello World!'),
    '#type' => 'markup',
  );
}
/**
 * Alter the render array that will make the confirm page for a single Entityform Type.
 * @param $render_array
 * @param $conf_eform_type
 * @param $conf_eform_id
 */
function hook_conf_eform_ENTITYFORM_TYPE_confirm_page_alter(&$render_array, $conf_eform_type, $conf_eform_id) {
  $render_array['new_markup'] = array(
    '#markup' => t('Hello 1 Form World!'),
    '#type' => 'markup',
  );
}

/**
 * Alter the Entityform Submission that is consider to be the previous submission for a user when submitting a form.
 *
 * For example:
 * @see conf_eform_anonymous_conf_eform_previous_submission_alter().
 *
 * @param object $conf_eform_submission
 *  The current previous submission if any.
 * @param string $conf_eform_type
 * @param array $context
 *  An associative array containing the following keys:
 *  - uid: uid of the user to find previous submissions
 */
function hook_conf_eform_previous_submission_alter(&$conf_eform_submission, $conf_eform_type, $context) {

}

/**
 * Allow altering fields automatically added to Entityform Views.
 *
 * conf_eform_settings will already have been added to the current View Display.
 * @see _conf_eform_view_add_all_fields().
 * @param $autofields
 *  Array of fields that will be added
 * @param $view
 * @param $display_id
 */
function hook_conf_eform_views_autofields_alter(&$autofields, $view, $display_id) {
  $view_conf_eform_settings = $view->display[$display_id]->conf_eform_settings;
  $instances = field_info_instances('conf_eform', $view_conf_eform_settings['conf_eform_type']);
  $view_mode = $view_conf_eform_settings['view_mode'];
  foreach ($autofields as &$autofield) {
    //Check to see this field was added by conf_eform
    if ($instances[$autofield['field_name']]) {
      $field = $instances[$autofield['field_name']];
      if ($label = _conf_eform_view_label_get_label($field, $view_mode)) {
        $autofield['options']['label'] = $label;
      }
    }
  }
}