<?php

/**
 * @file
 * Entityform i18n integration module via entity API i18n support.
 *
 * @see EntityDefaultI18nController
 */

/**
 * Implements hook_entity_info_alter().
 */
function conf_eform_i18n_entity_info_alter(&$info) {
  // Enable i18n support via the entity API.
  $info['conf_eform_type']['i18n controller class'] = 'EntityDefaultI18nStringController';
}

/**
 * Implements hook_i18n_object_info_alter().
 */
function conf_eform_i18n_i18n_object_info_alter(&$info) {
  if (empty($info['conf_eform_type']['string translation']['properties'])) {
    return;
  }
  $properties = &$info['conf_eform_type']['string translation']['properties'];
  foreach ($properties as $key => $property) {
    if (in_array($key, array('submission_text', 'instruction_pre'))) {
      $properties[$key]['format'] = "data.{$key}.format";
    }
  }
}

/**
 * Implements hook_i18n_string_list_TEXTGROUP_alter().
 */
function conf_eform_i18n_i18n_string_list_conf_eform_alter(&$strings, $type = NULL, $object = NULL) {
  if ($type == 'conf_eform_type' && $object && !empty($strings['conf_eform'][$type])) {
    foreach ($strings['conf_eform'][$type] as $conf_eform_type => $string_keys) {
      foreach ($string_keys as $string_key => $string) {
        if (empty($string['string'])) {
          $strings['conf_eform'][$type][$conf_eform_type][$string_key]['string'] = '';
          if (!empty($object->data[$string_key])) {
            if (is_array($object->data[$string_key]) && isset($object->data[$string_key]['value'])) {
              $strings['conf_eform'][$type][$conf_eform_type][$string_key]['string'] = $object->data[$string_key]['value'];
            }
            else {
              $strings['conf_eform'][$type][$conf_eform_type][$string_key]['string'] = $object->data[$string_key];
            }
          }
        }
      }
    }
  }
}

/**
 * Implements hook_conf_eform_type_insert().
 */
function conf_eform_i18n_conf_eform_type_insert($conf_eform_type) {
  i18n_string_object_update('conf_eform_type', $conf_eform_type);
}

/**
 * Implements hook_conf_eform_type_update().
 */
function conf_eform_i18n_conf_eform_type_update($conf_eform_type) {
  // Account for name changes.
  if ($conf_eform_type->original->type != $conf_eform_type->type) {
    i18n_string_update_context("conf_eform:conf_eform_type:{$conf_eform_type->original->type}:*", "conf_eform:conf_eform_type:{$conf_eform_type->type}:*");
  }
  i18n_string_object_update('conf_eform_type', $conf_eform_type);
}

/**
 * Implements hook_conf_eform_type_delete().
 */
function conf_eform_i18n_conf_eform_type_delete($conf_eform_type) {
  i18n_string_object_remove('conf_eform_type', $conf_eform_type);
}
