<?php

/**
 * @file
 * Entityform editing UI.
 *
 * We make very little use of the EntityAPI interface for this - preferring instead to use
 * views. That offers more flexibility to change a UI that will, more often than not,
 * be end-user facing.
 */

/**
 * UI controller.
 */
class EntityformUIController extends EntityDefaultUIController {

  /**
   * Overrides hook_menu() defaults. Main reason for doing this is that
   * parent class hook_menu() is optimized for entity type administration.
   */
  public function hook_menu() {
    //$conf_eform_base_path = variable_get('conf_eform_base_path', $)
    $items = array();
    $id_count = count(explode('/', $this->path));
    $front_id_count = count(explode('/', $this->entityInfo['admin ui']["front path"]));
    $wildcard = isset($this->entityInfo['admin ui']['menu wildcard']) ? $this->entityInfo['admin ui']['menu wildcard'] : '%' . $this->entityType;

    $items[$this->path] = array(
      'title' => 'Entityforms',
      'description' => 'Add edit and update conf_eforms.',
      'page callback' => 'system_admin_menu_block_page',
      'access arguments' => array('access administration pages'),
      'file path' => drupal_get_path('module', 'system'),
      'file' => 'system.admin.inc',
    );

    // Change the overview menu type for the list of conf_eforms.
    $items[$this->path]['type'] = MENU_LOCAL_TASK;
    $items['eform/submit/%conf_eform_type'] = array(
      'title callback' => 'conf_eform_page_title',
      'title arguments' => array(2, 1),
      'page callback' => 'conf_eform_type_submit_page',
      'page arguments' => array(2),
      'access callback' => 'conf_eform_access',
      'access arguments' => array('submit', 2),
      'file' => 'conf_eform.admin.inc',
      'file path' => drupal_get_path('module', $this->entityInfo['module']),
      'type' => MENU_CALLBACK,
    );
    $items['eform/%conf_eform_type/confirm'] = array(
      'title callback' => 'conf_eform_type_page_title',
      'title arguments' => array(1, 'confirm'),
      'description' => '',
      'page callback' => 'conf_eform_confirm_page',
      'page arguments' => array(1, 3),
      'access callback' => 'conf_eform_access',
      'access arguments' => array('confirm', 1),
    );

    $items['eform/%conf_eform_type/submissions'] = array(
      'title callback' => 'conf_eform_type_page_title',
      'title arguments' => array(1, 'view submissions'),
      'description' => '',
      'page callback' => 'conf_eform_submission_page',
      'page arguments' => array(1, 3, 'user'),
      'access arguments' => array('view own conf_eform'),
    );
    $types = array_keys(conf_eform_get_types());
    $controller = entity_ui_controller('conf_eform');

    // Loading and editing conf_eform entities.
    $items['conf_eform/' . $wildcard] = array(
      'page callback' => 'conf_eform_form_wrapper',
      'page arguments' => array($id_count + 1),
      'access callback' => 'conf_eform_access',
      'access arguments' => array('edit', $id_count + 1),
      'weight' => 0,
      'context' => MENU_CONTEXT_PAGE | MENU_CONTEXT_INLINE,
      'file' => 'conf_eform.admin.inc',
      'file path' => drupal_get_path('module', $this->entityInfo['module']),
    );
    $items['conf_eform/' . $wildcard . '/edit'] = array(
      'title' => 'Edit',
      'page callback' => 'conf_eform_form_wrapper',
      'page arguments' => array(1, 'edit'),
      'access callback' => 'conf_eform_access',
      'access arguments' => array('edit', 1),
      'weight' => 0,
      'type' => MENU_LOCAL_TASK,
      'context' => MENU_CONTEXT_PAGE | MENU_CONTEXT_INLINE,
      'file' => 'conf_eform.admin.inc',
      'file path' => drupal_get_path('module', $this->entityInfo['module']),
    );

    $items['conf_eform/' . $wildcard . '/delete'] = array(
      'title' => 'Delete',
      'page callback' => 'conf_eform_delete_form_wrapper',
      'page arguments' => array(1),
      'access callback' => 'conf_eform_access',
      'access arguments' => array('delete', 1),
      'type' => MENU_LOCAL_TASK,
      'context' => MENU_CONTEXT_PAGE | MENU_CONTEXT_INLINE,
      'weight' => 10,
      'file' => 'conf_eform.admin.inc',
      'file path' => drupal_get_path('module', $this->entityInfo['module']),
    );

    // Menu item for viewing conf_eforms.
    $items['conf_eform/' . $wildcard] = array(
      //'title' => 'Title',
      'title callback' => 'conf_eform_page_title',
      'title arguments' => array(1),
      'page callback' => 'conf_eform_page_view',
      'page arguments' => array(1),
      'access callback' => 'conf_eform_access',
      'access arguments' => array('view', 1),
      'type' => MENU_CALLBACK,
    );
    $items['conf_eform/' . $wildcard . '/view'] = array(
      'title' => 'View',
      'type' => MENU_DEFAULT_LOCAL_TASK,
      'weight' => -10,
    );
    return $items;
  }


  /**
   * Create the markup for the add Entityform Entities page within the class
   * so it can easily be extended/overriden.
   */
  public function addPage() {
    $item = menu_get_item();
    $content = system_admin_menu_block($item);

    if (count($content) == 1) {
      $item = array_shift($content);
      drupal_goto($item['href']);
    }

    return theme('conf_eform_add_list', array('content' => $content));
  }

  public function confirm_path($bundle, $conf_eform_id = NULL) {
    module_load_include('inc', 'conf_eform', 'conf_eform_type.admin');
    $conf_eform_type = conf_eform_type_load($bundle);
    if (!empty($conf_eform_id)) {
      $redirect_path = $conf_eform_type->get_path_property('redirect_path', conf_eform_load($conf_eform_id));
    }
    else {
      $redirect_path = $conf_eform_type->get_path_property('redirect_path');
    }
    if (!empty($redirect_path)) {
      return $redirect_path;
    }
    $path = _conf_eform_type_get_confirm_url($bundle);
    $path = drupal_get_path_alias($path);
    if ($conf_eform_id) {
      return array(
        $path,
        array(
          'query' => array(
            'conf_eform_id' => $conf_eform_id,
          ),
        ),
      );
    }
    return array($path);
  }

  public function submit_path($bundle) {
    module_load_include('inc', 'conf_eform', 'conf_eform_type.admin');
    $path = _conf_eform_type_get_submit_url($bundle);
    $path = drupal_get_path_alias($path);
    return $path;
  }


}

function conf_eform_type_submit_page($conf_eform_type) {
  $view = entity_view('conf_eform_type', array($conf_eform_type), 'full', NULL, TRUE);
  return $view;
}
/**
 * Form callback wrapper: create or edit a conf_eform.
 *
 * @param $conf_eform
 *   The conf_eform object being edited by this form.
 * @param $mode
 *     Current mode for this form
 *     Submit - user is submitting the form
 *     Edit - user with permission is editingform
 * @param $form_context
 *   How is form being used shown?
 *     'page' - on submit page
 *     'embedded' - called form EntityformTypeController->view()
 *     'preview' - Preview on Entityform type management
 * @see conf_eform_edit_form()
 */
function conf_eform_form_wrapper($conf_eform, $mode = 'submit', $form_context = 'page') {
  $make_form = TRUE;

  $conf_eform_type = conf_eform_type_load($conf_eform->type);

  if ($form_context == 'page') {
    drupal_set_title($conf_eform_type->getTranslation('label'));
  }
  if (!empty($conf_eform->is_new)) {

    // @todo add logic or other resubmit action.
    $old_submission = conf_eform_user_previous_submission($conf_eform_type->type);
    if ($old_submission) {
      $conf_eform = $old_submission;
      /*switch ($conf_eform_type->data['resubmit_action']) {
        case 'old':
          $conf_eform = $old_submission;
          break;
        case 'confirm':
          $confirm_path = entity_ui_controller('conf_eform')->confirm_path($old_submission->type, $old_submission->conf_eform_id);
          drupal_goto($confirm_path[0], $confirm_path[1]);
          break;
        case 'disallow':
          // @todo how should this be handled.
          $conf_eform_type->get_prop('disallow_resubmit_msg');
          $make_form = FALSE;
          break;
      }*/
    }

  }
  $output = array();
  /*if ($mode == 'submit' && (user_access('view own conf_eform') || user_access('view any conf_eform'))) {
    // Only show link if this user has a submission that will show up in the selected View.
    if (!empty($conf_eform_type->data['user_submissions_view'])) {
      $results = views_get_view_result($conf_eform_type->data['user_submissions_view'], NULL, $conf_eform_type->type);
      if (!empty($results)) {
        $output['submissions_link'] = array(
          '#type' => 'markup',
          '#markup' => "<div class='submissions-link' >" . l(t('View your previous submissions'), "eform/{$conf_eform_type->type}/submissions") . "</div>",
          '#weight' => -100,
        );
      }
    }
  }*/
  // Instructions get printed even if form is not created.
  if (!empty($conf_eform_type->data['instruction_pre'])) {
    $output['intro'] = array(
      '#type' => 'markup',
      '#markup' => "<div class='pre-instructions' >" . $conf_eform_type->get_prop('instruction_pre') . "</div>",
      '#weight' => -100,
    );
  }
  if ($make_form) {
    $form = drupal_get_form($conf_eform->type . '_conf_eform_edit_form', $conf_eform, $mode, $form_context);
    $form['#attributes']['class'][] = 'conf_eform';
    if (!empty($conf_eform->type)) {
      $form['#attributes']['class'][] = 'entitytype-' . $conf_eform->type . '-form';
    }
    $output += $form;
  }
  if (user_access('administer conf_eform types')) {
    // Make contextual links if user has access.
    $contextual_links = array();
    $contextual_links['conf_eform_edit'] = array(
      'admin/structure/conf_eform_types/manage',
      array($conf_eform_type->type),
    );
    $output['#contextual_links'] = $contextual_links;
  }
  return $output;
}


/**
 * Form callback wrapper: delete a conf_eform.
 *
 * @param $conf_eform
 *   The conf_eform object being edited by this form.
 *
 * @see conf_eform_edit_form()
 */
function conf_eform_delete_form_wrapper($conf_eform) {
  return drupal_get_form('conf_eform_delete_form', $conf_eform);
}


/**
 * Form callback: create or edit a conf_eform.
 *
 * @param $conf_eform
 *   The conf_eform object to edit or for a create form an empty conf_eform object
 *     with only a conf_eform type defined.
 */
function conf_eform_edit_form($form, &$form_state, $conf_eform, $mode = 'submit', $form_context = 'page') {
  global $user;
  $conf_eform_type = conf_eform_type_load($conf_eform->type);
  $is_review = FALSE;
  if (!empty($form_state['values']['op']) && $form_state['values']['op'] == t('Change')) {
    $conf_eform = $form_state['conf_eform_preview_entity'];
    unset($form_state['conf_eform_preview_entity']);
  }
  else {
    if (!empty($conf_eform_type->data['preview_page']) && !empty($form_state['conf_eform_preview_entity'])) {
      $conf_eform = $form_state['conf_eform_preview_entity'];
      $conf_eform->uid = $user->uid;
      $is_review = TRUE;
      $form['review'] = entity_get_controller('conf_eform')->view(array($conf_eform->conf_eform_id => $conf_eform), 'review', NULL, TRUE);
      drupal_set_message(t('Please review your submission'));
    }
  }
  // Add the field related form elements.
  $form_state['conf_eform'] = $conf_eform;
  $form_state['conf_eform_form_mode'] = $mode;
  if (!$is_review) {
    field_attach_form('conf_eform', $conf_eform, $form, $form_state);
  }

  $form['actions'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('form-actions')),
    '#weight' => 400,
  );

  // We add the form's #submit array to this button along with the actual submit
  // handler to preserve any submit handlers added by a form callback_wrapper.
  $submit = array();

  if (!empty($form['#submit'])) {
    $submit += $form['#submit'];
  }
  if ($mode == 'submit') {
    // Determine if we are on Preview Form page.
    if ($is_review) {
      $form['actions']['change'] = array(
        '#type' => 'submit',
        '#value' => t('Change'),
        '#conf_eform_change' => 1,
        '#submit' => $submit + array('conf_eform_edit_form_submit'),
      );
    }
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => $conf_eform_type->get_prop('submit_button_text'),
      '#submit' => $submit + array('conf_eform_edit_form_submit'),
      '#conf_eform_after_review' => $is_review,
    );
  }
  else {
    $form['user_info'] = array(
      '#type' => 'markup',
      '#markup' => _conf_eform_get_submit_info($conf_eform),
      '#weight' => -200,
      '#prefix' => '<div class="submitted">',
      '#suffix' => '</div>',
    );
    $form['actions']['save'] = array(
      '#type' => 'submit',
      '#value' => t('Save Changes'),
      '#submit' => $submit + array('conf_eform_edit_form_submit'),
    );
  }
  if (!empty($conf_eform->name)) {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete conf_eform'),
      '#suffix' => l(t('Cancel'), 'admin/structure/conf_eforms'),
      '#submit' => $submit + array('conf_eform_form_submit_delete'),
      '#weight' => 45,
    );
  }

  // We append the validate handler to #validate in case a form callback_wrapper
  // is used to add validate handlers earlier.
  $form['#validate'][] = 'conf_eform_edit_form_validate';
  // Make sure the proper files are loaded if the form has an ajax callback.
  // This happens in image field upload for instance.
  $form['#after_build'][] = 'conf_eform_load_required_conf_eform_admin';
  $form['#after_build'][] = 'conf_eform_clear_required';
  $form['#validate'][] = 'conf_eform_set_required';
  if ($form_context == 'preview') {
    _conf_eform_type_modify_preview_form($form);
  }

  return $form;
}

/**
 * Modify an Entityform add/edit form to use as a preview.
 * @param $form
 */
function _conf_eform_type_modify_preview_form(&$form) {
  foreach (element_children($form['actions']) as $child_key) {
    if ($form['actions'][$child_key]['#type'] == 'submit') {
      $form['actions'][$child_key]['#disabled'] = TRUE;
      $form['actions'][$child_key]['#executes_submit_callback'] = FALSE;
      unset($form['actions'][$child_key]['#submit']);
    }
  }
}

/**
 * Called via after_build on conf_eforms.
 * This makes sure that required fields aren't required when saving a Draft.
 * A Draft is not complete so it shouldn't enforce required fields.
 *
 * @param array $form
 * @param array $form_state
 * @return array
 */
function conf_eform_clear_required($form, &$form_state) {
  if (!empty($form_state['triggering_element']['#conf_eform_draft'])) {
    _conf_eform_set_all_nested_props($form, array('#required' => FALSE));
  }
  return $form;
}

/**
 * @param array $form
 * @param array $props
 *   properties to set
 * @param boolean $must_match
 */
function _conf_eform_set_all_nested_props(&$form, $props, $must_match = FALSE) {
  $matched = TRUE;
  if ($must_match) {
    $matched = isset($form['#conf_eform_required']);
  }
  if ($matched) {
    foreach ($props as $key => $value) {
      if (isset($form[$key]) && $form[$key] != $value) {
        $form[$key] = $value;
        if (!$must_match) {
          $form['#conf_eform_required'] = TRUE;
        }
        else {
          unset($form['#conf_eform_required']);
        }
      }
    }
  }

  foreach (element_children($form) as $key) {
    if (is_array($form[$key])) {
      _conf_eform_set_all_nested_props($form[$key], $props, $must_match);
    }
  }
}

/*
 * Set form elements back to required.
 */
function conf_eform_set_required(&$form, &$form_state) {
  _conf_eform_set_all_nested_props($form, array('#required' => TRUE), TRUE);
}

/**
 * Form API validate callback for the conf_eform form.
 */
function conf_eform_edit_form_validate(&$form, &$form_state) {
  $conf_eform = $form_state['conf_eform'];

  // Notify field widgets to validate their data.
  field_attach_form_validate('conf_eform', $conf_eform, $form, $form_state);
}


/**
 * Form API submit callback for the conf_eform form.
 *
 * @todo remove hard-coded link
 */
function conf_eform_edit_form_submit(&$form, &$form_state) {

  $conf_eform = entity_ui_controller('conf_eform')->entityFormSubmitBuildEntity($form, $form_state);
  // Add in created and changed times. This must be added before preview to get time.
  if ($conf_eform->is_new = isset($conf_eform->is_new) ? $conf_eform->is_new : 0) {
    global $user;
    $conf_eform->created = time();

    $conf_eform->uid = !empty($user->uid) ? $user->uid : 0;
  }

  $conf_eform->changed = time();
  // Save the conf_eform and go back to the list of conf_eforms.
  $conf_eform_type = conf_eform_type_load($conf_eform->type);
  if (!empty($form_state['clicked_button']['#conf_eform_change'])) {
    $form_state['conf_eform_preview_entity'] = $conf_eform;
    $form_state['rebuild'] = TRUE;
    drupal_set_message(t('Re-enter'));
    return;
  }
  else {
    // If this conf_eform type has preview mode and we are in submit store preview.
    if (empty($form_state['clicked_button']['#conf_eform_after_review'])
      && !empty($conf_eform_type->data['preview_page'])
      && $form_state['conf_eform_form_mode'] == 'submit') {
      $form_state['conf_eform_preview_entity'] = $conf_eform;
      $form_state['rebuild'] = TRUE;
      return;
    }
  }

  $conf_eform->save();
  if ($form_state['conf_eform_form_mode'] == 'submit' || !user_access('edit any conf_eform')) {
    $redirect_path = $conf_eform_type->get_path_property('redirect_path', $conf_eform);
    if (!empty($redirect_path)) {
      $form_state['redirect'] = $redirect_path;
    }
    else {
      global $user;
      if (empty($user->uid)) {
        // For anonymous users we must store the id of their submission in the session.
        drupal_session_start();
        $_SESSION['conf_eform_submission'] = $conf_eform->conf_eform_id;
      }
      $confirm_path = entity_ui_controller('conf_eform')->confirm_path($conf_eform->type, $conf_eform->conf_eform_id);
      $form_state['redirect'] = array($confirm_path[0], $confirm_path[1]);
    }

    drupal_set_message($conf_eform_type->get_prop('submit_confirm_msg', $conf_eform));
  }
  else {
    $info = entity_get_info('conf_eform_type');
    $form_state['redirect'] = $info['admin ui']['path'] . "/manage/{$conf_eform_type->type}/submissions";
    drupal_set_message($conf_eform_type->get_prop('submit_confirm_msg', $conf_eform));
  }
}

/**
 * Form API submit callback for the delete button.
 *
 * @todo Remove hard-coded path
 */
function conf_eform_form_submit_delete(&$form, &$form_state) {
  $form_state['redirect'] = 'admin/structure/conf_eforms/conf_eform/' . $form_state['conf_eform']->conf_eform_id . '/delete';
}


/**
 * Form callback: confirmation form for deleting a conf_eform.
 *
 * @param $conf_eform
 *   The conf_eform to delete
 *
 * @see confirm_form()
 */
function conf_eform_delete_form($form, &$form_state, $conf_eform) {
  $form_state['conf_eform'] = $conf_eform;

  $form['#submit'][] = 'conf_eform_delete_form_submit';
  $conf_eform_type = conf_eform_type_load($conf_eform->type);
  // @todo Where should a non-admin go when canceling?
  $form = confirm_form($form,
    $conf_eform_type->get_prop('delete_confirm_msg', $conf_eform),
    "admin/structure/conf_eform_types/manage/{$conf_eform->type}/submissions",
    '<p>' . t('This action cannot be undone.') . '</p>',
    t('Delete'),
    t('Cancel'),
    'confirm'
  );

  return $form;
}

/**
 * Submit callback for conf_eform_delete_form.
 */
function conf_eform_delete_form_submit($form, &$form_state) {
  $conf_eform = $form_state['conf_eform'];

  conf_eform_delete($conf_eform);

  drupal_set_message(t('The Submission for %name has been deleted.', array('%name' => $conf_eform->getTypeName())));
  watchdog('conf_eform', 'Deleted Submission for %name.', array('%name' => $conf_eform->getTypeName()));

  $form_state['redirect'] = "admin/structure/conf_eform_types/manage/{$conf_eform->type}/submissions";
}


/**
 * Page to add Entityform Entities.
 *
 * @todo Pass this through a proper theme function
 */
function conf_eform_add_page() {
  $controller = entity_ui_controller('conf_eform');
  return $controller->addPage();
}


/**
 * Displays the list of available conf_eform types for conf_eform creation.
 *
 * @ingroup themeable
 */
function theme_conf_eform_add_list($variables) {
  $content = $variables['content'];
  $output = '';
  if ($content) {
    $output = '<dl class="conf_eform-type-list">';
    foreach ($content as $item) {
      $output .= '<dt>' . l($item['title'], $item['href']) . '</dt>';
      $output .= '<dd>' . filter_xss_admin($item['description']) . '</dd>';
    }
    $output .= '</dl>';
  }
  else {
    if (user_access('administer conf_eform types')) {
      $output = '<p>' . t('Entityform Entities cannot be added because you have not created any conf_eform types yet. Go to the <a href="@create-conf_eform-type">conf_eform type creation page</a> to add a new conf_eform type.', array('@create-conf_eform-type' => url('admin/structure/conf_eform_types/add'))) . '</p>';
    }
    else {
      $output = '<p>' . t('No conf_eform types have been created yet for you to use.') . '</p>';
    }
  }

  return $output;
}
