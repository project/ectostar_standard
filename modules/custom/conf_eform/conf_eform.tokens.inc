<?php
/**
 * @file Token related hooks.
 */
/**
 * Implements hook_token_info().
 */
function conf_eform_token_info() {

  $tokens['submit_url'] = array(
    'name' => t('Submission URL'),
    'description' => t('Submit URL for Entityform type.'),
  );
  return array(
    'tokens' => array('conf_eform_type' => $tokens),
  );
}

/**
 * Implements hook_tokens().
 */
function conf_eform_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $return = array();
  if ($type == 'conf_eform_type' && !empty($data['conf_eform_type'])) {
    $conf_eform_type = $data['conf_eform_type'];
    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'submit_url':
          $return[$original] = url(_conf_eform_type_get_submit_url($conf_eform_type->type), array(
            'absolute' => TRUE));
          break;

      }
    }
  }

  return $return;
}